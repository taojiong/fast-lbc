#include "fastHBCSolver.h"
#include <iostream>
#include <algorithm>
#include <fstream>
#include <MatOp/SparseGenMatProd.h>
#include <MatOp/DenseGenMatProd.h>
#include <GenEigsSolver.h>
#include "monty.h"
#include "fusion.h"
using namespace mosek::fusion;
using namespace monty;

namespace LBC {

    void fastHBCSolver::init()
    {
        convergence_check_frequency_ = std::max(1, param_.convergence_check_frequency);
        output_frequency_ = std::max(1, param_.output_frequency_ratio * convergence_check_frequency_);
        init_variable();
        init_threshold();
        init_linear_constraints();
    }
    void fastHBCSolver::init_variable()
    {
        dim_ = control_points_.rows();
        n_sample_points_ = sample_points_.cols();
        n_control_points_ = control_points_.cols();
        n_data_points_ = data_points_.cols();
        n_cells_ = E_.cols() / dim_ ;
        n_cell_edges_ = 3;
        n_edges_ = edge_vertices_.cols();
        n_inner_edges = inner_edges_.size();
        n_cell_grad_ = n_cells_ * n_control_points_;
        G_.setZero(dim_ * n_cells_, n_control_points_);   // initialize G with zero
        Lambda_.setZero(3 * n_cells_, n_control_points_); // initialize Lambda with zer
        previous_D = D_;
        EG_.resize(3 * n_cells_, n_control_points_);
        // compute the eigen value and eigen vector
        eigvalue.resize(2,  n_cells_);
        eigvector.resize(2, 2 * n_cells_);
        E.resize(2, 3 * n_cells_);
      //  E.resize(3, 2* n_cells_);
        EtE.resize(2, 2 * n_cells_);
        VE.resize(2, 3 * n_cells_);
        for(int i = 0; i < n_cells_; i ++)
        {
            DenseMatrix E1 = E_.block(3 * i, 2 * i, 3, 2);
            E.block(0, 3 * i , 2, 3) = E1.transpose();
           // E.block(0, 2 * i, 3, 2) = E1;
            DenseMatrix A = E1.transpose() * E1;
            EtE.block(0, 2 * i, 2, 2) = A;
            Eigen::EigenSolver<DenseMatrix> es(A);
            Eigen::MatrixXd D = es.eigenvalues().real();
            Eigen::Matrix2d V = es.eigenvectors().real();
            eigvalue.col(i) =  D;
            eigvector.block(0, 2 * i, 2, 2) = V;
            VE.block(0, 3 * i, 2, 3) = V.transpose() * E1.transpose();
        }

        // compute the coefficient (alpha) in updte - G subproblem
        Alpha_.resize(n_cells_, n_control_points_);
        for(int i = 0; i < n_cell_grad_; i ++)
        {
            int f_index = i % n_cells_;
            int c_index = i / n_cells_;
           // Alpha_(f_index, c_index) = grad_weights_(f_index, c_index) * A_(f_index);
            Alpha_(f_index, c_index) = A_(f_index);
        }
        // init coefficient matrix for reconstruction

        int max_index_boundary_point = n_sample_points_ - n_data_points_;
        Coef_.resize(n_inner_edges, n_data_points_);
        std::vector<TripletD> triplet_coef ;

        for(int i = 0; i < n_inner_edges; i ++)
        {
            int index_p1,index_p2;
            index_p1 = edge_vertices_(0, inner_edges_[i]);
            index_p2 = edge_vertices_(1, inner_edges_[i]);

            if(index_p1 < max_index_boundary_point && index_p2 >= max_index_boundary_point)
            {
                triplet_coef.push_back(TripletD(i, index_p2 - max_index_boundary_point, -1.0));
            }
            if(index_p1 >= max_index_boundary_point && index_p2 >= max_index_boundary_point)
            {
                triplet_coef.push_back(TripletD(i, index_p1 - max_index_boundary_point, 1.0));
                triplet_coef.push_back(TripletD(i, index_p2 - max_index_boundary_point, -1.0));
            }
        }

        Coef_.setFromTriplets(triplet_coef.begin(),triplet_coef.end());
        Coef_solver.compute(Coef_.transpose() * Coef_);
    }

    void fastHBCSolver::init_threshold()
    {
        double primal_threshold = std::max(param_.abs_eps, n_cell_edges_ * n_cells_ * n_control_points_ * param_.rel_primal_eps);
        double dual_threshold =   std::max(param_.abs_eps, n_edges_ * n_control_points_ * param_.rel_dual_eps);
        primal_residual_sqr_norm_threshold_ =  primal_threshold * primal_threshold;
        dual_residual_sqr_norm_threshold_ =    dual_threshold * dual_threshold;
        std::cout <<"prime threshold : " << primal_residual_sqr_norm_threshold_ << std::endl;
        std::cout << "dual threshold : " << dual_residual_sqr_norm_threshold_ << std::endl;
    }
    void fastHBCSolver::init_linear_constraints()
    {
        Kt.resize(dim_ + 1, n_control_points_);

        Kt.block(0, 0, dim_, n_control_points_) = control_points_;
        Kt.row(dim_).fill(1.0);

        Eigen::JacobiSVD<DenseMatrix, Eigen::FullPivHouseholderQRPreconditioner> jsvd(Kt, Eigen::ComputeFullU | Eigen::ComputeFullV);
        svd_Kt.compute(Kt, Eigen::ComputeFullU | Eigen::ComputeFullV);

        int nrank = svd_Kt.nonzeroSingularValues();
        M_ = jsvd.matrixV().block(0, nrank, n_control_points_, n_control_points_ - nrank);
        Mt_ = M_.transpose();


        svd_M.compute(M_, Eigen::ComputeFullU | Eigen::ComputeFullV);
        QR_M.compute(M_);
        LDLT_M.compute(Mt_ * M_);
        //compute the d_jk_prime in update - D subproblem
         D_prime.resize(n_inner_edges, n_control_points_);
        for(int i = 0 ; i < n_inner_edges; i ++)
        {
            int e_index,p1_index,p2_index;
            Vector3d b;
            e_index = inner_edges_[i];
            p1_index =  edge_vertices_(0, e_index);
            p2_index =  edge_vertices_(1, e_index);

            b(0) = sample_points_(0, p1_index) - sample_points_(0, p2_index);
            b(1) = sample_points_(1, p1_index) - sample_points_(1, p2_index);
            b(2) = 0;
            D_prime.block(i, 0, 1, n_control_points_) = svd_Kt.solve(b).transpose();
        }
    }


    void fastHBCSolver::update_G()
    {
      #pragma omp for
        //double begin_time;
       //double finish_time;

       // begin_time = omp_get_wtime();
        for(int i = 0; i < n_cell_grad_; i ++)
        {

            int f_index = i % n_cells_;\
            int R_index = 3 * f_index;
            int r_index = 2 * f_index;
            int c_index = i / n_cells_;



            double alpha = Alpha_(f_index, c_index);
            double mu = param_.penalty_weight;
            double lambda = alpha / mu;
            Vector3d f;
            Vector2d g;

            for(int j = 0; j < 3 ; j ++)
            {
              f(j) = D_(cell_edges_(f_index, j), c_index) - Lambda_(R_index + j, c_index) / mu;
            }

            Vector2d  b = E.block(0, R_index, 2, 3) * f;

            double A00 = lambda + EtE(0, r_index);
            double A01 = EtE(0, r_index + 1);
            double A10 = EtE(1, r_index);
            double A11 = lambda + EtE(1, r_index + 1);
            double det = A00 * A11 - A10 * A01;
            g(0) = (b(0) * A11 - b(1) * A01) / det;
            g(1) = (b(1) * A00 - b(0) * A10) / det;

            G_.block(r_index, c_index, dim_, 1) =  g;
       }
        //finish_time = omp_get_wtime();

       // std::cout << "g time : " << std::max(0.0, finish_time - begin_time) << std::endl;
    }
    void fastHBCSolver :: update_D()
    {
      /*   #pragma omp for
        for(int i = 0; i < 3 * n_cells_; i ++)
        {
            int cell_index = i / 3;
            int edge_index = i % 3;
            EG_.row(i) = param_.relaxation_alpha* E_.block(i, 2 * cell_index, 1, 2) * G_.block(2 * cell_index, 0, 2, n_control_points_)
                    - (param_.relaxation_alpha - 1) * D_.row(cell_edges_(cell_index, edge_index));
        }*/

        //#pragma omp parallel for num_threads(8)
#pragma omp for
       // double begin_time;
       // double finish_time;
       // begin_time = omp_get_wtime();
        for(int i = 0 ; i < n_inner_edges; i ++)
        {
            double mu = 1 / param_.penalty_weight;
           // DenseVector d_jk_prime, d_jk, y_jk;
           // DenseVector d_jk_prime, y_jk;
            DenseVector d_jk_prime;
            DenseVector sum_jk;
            DenseVector y_jk;
            //DenseMatrix G_jk, e_jk;
            sum_jk.setZero(n_control_points_, 1);
           // d_jk_prime = D_prime.row(i).transpose();
            for(int j = 0; j < 2; j ++)
            {
              int c_index = edge_E_(inner_edges_[i], j);
              int r_index = c_index / 3  * 2;
             // G_jk = G_.block(r_index, 0, 2, n_control_points_);
             // e_jk = E_.block(c_index, r_index, 1, 2);
            //  sum_jk +=  (e_jk * G_jk + 1.0 / mu * Lambda_.row(c_index)).transpose();
              double e1 = E_.coeff(c_index, r_index);
              double e2 = E_.coeff(c_index, r_index + 1);
               sum_jk += (e1 * G_.row(r_index) + e2 * G_.row(r_index + 1) + mu * Lambda_.row(c_index)).transpose();
              //  sum_jk += (EG_.row(c_index) + mu * Lambda_.row(c_index)).transpose();
             // sum_jk = alpha_jk + (mu * Lambda_.row(c_index)).transpose();
            }

           //  y_jk = svd_M.solve(0.5 * sum_jk - d_jk_prime);

            // D_.row(inner_edges_[i]) = (d_jk_prime + M_ * y_jk).transpose();
            y_jk = 0.5 * sum_jk;
            D_.row(inner_edges_[i]) = y_jk.transpose();
         }
        //finish_time = omp_get_wtime();
        //std::cout << "d time : " << std::max(0.0, finish_time - begin_time) << std::endl;
    }

    void fastHBCSolver::update_dual_variables(int iter_num)
    {
    /*#pragma omp sections
        {
       #pragma omp section
          {
            if(check_convergence_)
              {
                 primal_residual_ = (E_ * G_ - S_ * D_);
               }

          }
       #pragma omp section
          {
            if(check_convergence_)
             {
                dual_residual_ = param_.penalty_weight * (D_ - previous_D);
              }
           }
       #pragma omp section
         {
            Lambda_ += param_.penalty_weight*(EG_ - S_ * D_);
           }
        }*/

    #pragma omp single
        {
            primal_residual_ = E_ * G_ - S_ * D_;
            Lambda_ += param_.penalty_weight *(primal_residual_);
        if(check_convergence_){
             // primal_residual_ = E_ * G_ - S_ * D_;
              dual_residual_ = param_.penalty_weight * (D_ - previous_D);
              primal_residual_sqr_norm_ = (primal_residual_).squaredNorm();
              dual_residual_sqr_norm_   =(dual_residual_).squaredNorm();
              optimization_converge_ = ( primal_residual_sqr_norm_ <= primal_residual_sqr_norm_threshold_
                        && dual_residual_sqr_norm_ <= dual_residual_sqr_norm_threshold_ );

              optimization_end_ = optimization_converge_ || iter_num >= param_.max_iterations;

                if(optimization_converge_){
                    std::cout << "Solver converged." << std::endl;

                }
                else if(optimization_end_){
                    std::cout << "Maximum iteration reached." << std::endl;
                }

                if (output_progress_ || optimization_end_)
                {
                    restore_prime_residual.push_back(primal_residual_sqr_norm_);
                    restore_dual_residual.push_back(dual_residual_sqr_norm_);
                    std::cout << "Iteration " << iter_num << ":" << std::endl;
                    std::cout << "Primal residual squared norm: " << primal_residual_sqr_norm_ << ",  primal threshold:" << primal_residual_sqr_norm_threshold_ << std::endl;
                    std::cout << "Dual residual squared norm: " << dual_residual_sqr_norm_ << ",  dual threshold:" << dual_residual_sqr_norm_threshold_ << std::endl;
                   // get_final_coordinates();

                    end_time = omp_get_wtime();
                    cost_time.push_back(std::max(0.0, end_time - start_time));
                   // std::cout << "cost time : " << end_time - start_time << std::endl;
                }
            }
    }
    }

    void fastHBCSolver ::solve()
    {
        start_timer();
       // std::ofstream in;
      //  in.open("fastlbcmodel1_over.txt", std::ios::trunc); // open txt
        int iter = 0;
        optimization_end_ = false;
        \
        double begin_time ,finish_time;
        start_time = omp_get_wtime();

       while (!optimization_end_)
        {
            iter++;
            check_convergence_ = (iter % convergence_check_frequency_ == 0 || iter >= param_.max_iterations);
            output_progress_ = (iter % output_frequency_ == 0);
            previous_D = D_;
           #pragma omp parallel
            {
                //begin_time = omp_get_wtime();
                this -> update_G();
              //  finish_time = omp_get_wtime();
             //   std::cout << "update g time : " << std::max(0.0, finish_time - begin_time) << std::endl;
             //   begin_time = omp_get_wtime();
                this -> update_D();
             //   finish_time = omp_get_wtime();
            //    std::cout << "update d time : " << std::max(0.0, finish_time - begin_time) << std::endl;
             //   begin_time = omp_get_wtime();
                this -> update_dual_variables(iter);
             //   finish_time = omp_get_wtime();
             //   std::cout << "update dual time : " << std::max(0.0, finish_time - begin_time) << std::endl;
            }

           /* if(output_progress_ || optimization_end_)
            {
                for(int i = 0; i < Z_.rows(); i ++)
                {
                    for(int j = 0; j < Z_.cols(); j ++)
                    {
                        in << Z_(i, j) << "\n";

                    }
                }
                std::cout << "number of data : " << Z_.rows() * Z_.cols() << std::endl;
            }*/
        }
        //in.close();  //close txt

        end_timer();
        show_elapsed_time();
        get_final_coordinates();

        std::ofstream in;
        in.open("fastlbcmodel3_over_time.txt", std::ios::trunc);
        for(int i = 0; i < cost_time.size(); i ++)
        {
            in << cost_time[i] << "\n";
        }

        in.close();


    }

    void fastHBCSolver::get_final_coordinates()
    {
        int max_index_boundary_point = n_sample_points_ - n_data_points_;
        DenseMatrix  b(n_inner_edges, n_control_points_);
        b.fill(0.0);
        for(int i = 0; i < n_inner_edges; i ++)
        {
            int index_p1,index_p2;
            index_p1 =std::min(edge_vertices_(0 , inner_edges_[i]), edge_vertices_(1, inner_edges_[i]));
            index_p2 =std::max(edge_vertices_(0 , inner_edges_[i]), edge_vertices_(1, inner_edges_[i]));
            if(index_p1 < max_index_boundary_point && index_p2 >= max_index_boundary_point)
            {
                b.row(i) = D_.block(inner_edges_[i], 0, 1, n_control_points_) - init_coord_.row(index_p1);
            }
            if(index_p1 >= max_index_boundary_point && index_p2 >= max_index_boundary_point)
            {
                b.row(i) = D_.block(inner_edges_[i], 0, 1, n_control_points_);
            }
        }

        Z_ = Coef_solver.solve(Coef_.transpose() * b);
        for(int i = 0 ; i < Z_.rows(); i++)
        {
            std::cout << "test : " << Z_.row(i).sum() << std::endl;
        }
    }


    void fastHBCSolver:: compute_energy()
    {
        double energy = 0;
        DenseMatrix gradient = G_;
        DenseMatrix residual = E_ * G_ - S_ * D_ ;
        double l2_norm ;
        for(int i = 0; i < n_cells_; i ++)
        {
            for(int j = 0; j < n_control_points_; j ++)
            {
                l2_norm = sqrt((pow(gradient(2 * i , j), 2) + pow(gradient(2 * i + 1 ,j), 2)));
                energy = energy + grad_weights_(i, j) * l2_norm * A_(i) ;
            }
        }
        for(int i =0; i < 3 * n_cells_; i ++)
        {
            for(int j = 0; j < n_control_points_; j ++)
            {
                energy = energy + Lambda_(i, j) * residual(i, j);
            }
        }
        energy = energy + param_.penalty_weight * residual.squaredNorm();

        std::cout << "energy : " << energy << std::endl;
    }

    void fastHBCSolver::start_timer(){
        if(param_.use_timer){
            start_time_ = omp_get_wtime();
        }
    }

    void fastHBCSolver::end_timer(){
        if(param_.use_timer){
            end_time_ = omp_get_wtime();
        }
    }

    void fastHBCSolver::show_elapsed_time(){
        if(param_.use_timer){
            std::cout << "Solving time: " << std::max(0.0, end_time_ - start_time_) << " seconds." << std::endl;
        }
    }
}


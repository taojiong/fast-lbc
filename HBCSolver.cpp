#include "HBCSolver.h"
namespace LBC {

void HBCSolver::init_variables()
{
   n_data_points_ = data_points_.cols();
   n_control_points_ = control_points_.cols();
   n_sample_points_ = sample_points_.cols();
}

void HBCSolver::solve()
{
    Coef_.resize(n_data_points_, n_data_points_);
    b_.resize(n_data_points_, n_control_points_);
    Z_.resize(n_data_points_, n_control_points_);
    b_.fill(0);
    int max_boundary_point_index = n_sample_points_ - n_data_points_;
    std::vector<TripletD> triplet_coef;
    for(int i = max_boundary_point_index; i < n_sample_points_; i++)
    {
        int num_neighbour = vertices_neighbour_[i].size();
        for(int j = 0 ; j < num_neighbour; j ++)
        {
            int index_neighbour = vertices_neighbour_[i][j];
            if(index_neighbour < max_boundary_point_index)
            {
              b_.row(i - max_boundary_point_index) -= init_coord_.row(index_neighbour);
            }
            else
            {
              triplet_coef.push_back(TripletD(i - max_boundary_point_index, index_neighbour - max_boundary_point_index, 1.0));
            }
        }
        triplet_coef.push_back(TripletD(i - max_boundary_point_index, i - max_boundary_point_index, -num_neighbour));
    }
    Coef_.setFromTriplets(triplet_coef.begin(), triplet_coef.end());

    Eigen::SimplicialLDLT<SparseMatrix> ldlt;
    ldlt.compute(Coef_);
    Z_ = ldlt.solve(b_);


    double begin_time = omp_get_wtime();
    Eigen::ConjugateGradient<SparseMatrix, Eigen::Lower|Eigen::Upper> cg;
    cg.compute(Coef_);
    Z_ = cg.solve(b_);
    std::cout << "iterations : " << cg.iterations() << std::endl;
    std::cout << "error : " << cg.error() << std::endl;
    double finish_time = omp_get_wtime();

    std::cout << "cost time : " << std::max(0.0, finish_time - begin_time) << std::endl;
}
}


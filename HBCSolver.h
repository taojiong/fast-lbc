#ifndef HBCSOLVER_H
#define HBCSOLVER_H

#include <common.h>
#include <DataSetup.h>
#include <algorithm>

namespace LBC{
class HBCSolver
{
public:
    HBCSolver(const DataSetup& data_setup):
        sample_points_(data_setup.get_sample_point()),
        control_points_(data_setup.get_LBC_solver_control_points()),
        data_points_(data_setup.get_LBC_solver_data_points()),
        edge_vertices_(data_setup.get_edge_vertces()),cell_edges_(data_setup.get_cell_edges()),
        inner_edges_(data_setup.get_inner_edge()),
        vertices_neighbour_(data_setup.get_vertices_neighbour()),
        init_coord_(data_setup.get_init_coord())
   {
        init_variables();
    }
virtual~ HBCSolver(){}
const DenseMatrix& get_coordinates() const{
    return Z_;
}
void solve();

protected:

    DenseMatrix control_points_,data_points_,sample_points_;
    DenseIndexMatrix edge_vertices_;
    DenseIndexMatrix cell_edges_;
    std::vector<int> inner_edges_;
    std::vector<std::vector<int>> vertices_neighbour_;

    DenseMatrix   init_coord_;

    SparseMatrix Coef_; // Laplace matrix;
    DenseMatrix  b_;   // the rhs of laplace equation
    DenseMatrix  Z_;   // final harmonic coordinates

    // problem dimensions
    int dim_;
    int n_sample_points_;
    int n_control_points_;
    int n_data_points_;
    int n_cells_;
    int n_edges_;
    int n_inner_edges;
    int n_cell_edges_;
    // Cholesky solver for the linear system
    #ifdef USE_CHOLMOD
    Eigen::CholmodDecomposition<SparseMatrix> solver_;
    #else
    Eigen::LDLT<DenseMatrix> solver_Coef;
    #endif
    void init_variables();

};
}

#endif // HBCSOLVER_H

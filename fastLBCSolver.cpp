#include "fastLBCSolver.h"
#include <iostream>
#include <algorithm>
#include <fstream>
#include <MatOp/SparseGenMatProd.h>
#include <MatOp/DenseGenMatProd.h>
#include <GenEigsSolver.h>
#include "monty.h"
#include "fusion.h"
using namespace mosek::fusion;
using namespace monty;


namespace LBC {

    void fastLBCSolver::init()
    {
        convergence_check_frequency_ = std::max(1, param_.convergence_check_frequency);
        output_frequency_ = std::max(1, param_.output_frequency_ratio * convergence_check_frequency_);
        init_variable();
        init_threshold();
        init_linear_constraints();
    }
    void fastLBCSolver::init_variable()
    {
        dim_ = control_points_.rows();
        n_sample_points_ = sample_points_.cols();
        n_control_points_ = control_points_.cols();
        n_data_points_ = data_points_.cols();
        n_cells_ = E_.cols() / dim_ ;
        n_cell_edges_ = 3;
        n_edges_ = edge_vertices_.cols();
        n_inner_edges = inner_edges_.size();
        n_cell_grad_ = n_cells_ * n_control_points_;
        G_.setZero(dim_ * n_cells_, n_control_points_);   // initialize G with zero
        Lambda_.setZero(3 * n_cells_, n_control_points_); // initialize Lambda with zer
        previous_D = D_;
        EG_.resize(3 * n_cells_, n_control_points_);
        // compute the eigen value and eigen vector
        eigvalue.resize(2,  n_cells_);
        eigvector.resize(2, 2 * n_cells_);
        VE.resize(2, 3 * n_cells_);
        for(int i = 0; i < n_cells_; i ++)
        {
            DenseMatrix E = E_.block(3 * i, 2 * i, 3, 2);
            DenseMatrix A = E.transpose() * E;
            Eigen::EigenSolver<DenseMatrix> es(A);
            Eigen::MatrixXd D = es.eigenvalues().real();
            Eigen::Matrix2d V = es.eigenvectors().real();
            eigvalue.col(i) =  D;
            eigvector.block(0, 2 * i, 2, 2) = V;
            VE.block(0, 3 * i, 2, 3) = V.transpose() * E.transpose();
        }

        // compute the coefficient (alpha) in updte - G subproblem
        Alpha_.resize(n_cells_, n_control_points_);
        for(int i = 0; i < n_cell_grad_; i ++)
        {
            int f_index = i % n_cells_;
            int c_index = i / n_cells_;
            Alpha_(f_index, c_index) = grad_weights_(f_index, c_index) * A_(f_index);
        }
        // init coefficient matrix for reconstruction

        int max_index_boundary_point = n_sample_points_ - n_data_points_;
        Coef_.resize(n_inner_edges, n_data_points_);
        std::vector<TripletD> triplet_coef ;

        for(int i = 0; i < n_inner_edges; i ++)
        {
            int index_p1,index_p2;
            index_p1 = edge_vertices_(0, inner_edges_[i]);
            index_p2 = edge_vertices_(1, inner_edges_[i]);

            if(index_p1 < max_index_boundary_point && index_p2 >= max_index_boundary_point)
            {
                triplet_coef.push_back(TripletD(i, index_p2 - max_index_boundary_point, -1.0));
            }
            if(index_p1 >= max_index_boundary_point && index_p2 >= max_index_boundary_point)
            {

                triplet_coef.push_back(TripletD(i, index_p1 - max_index_boundary_point, 1.0));

                triplet_coef.push_back(TripletD(i, index_p2 - max_index_boundary_point, -1.0));
            }
        }

        Coef_.setFromTriplets(triplet_coef.begin(),triplet_coef.end());
        Coef_solver.compute(Coef_.transpose() * Coef_);
    }
    
    void fastLBCSolver::init_threshold()
    {
        double primal_threshold = std::max(param_.abs_eps, n_cell_edges_ * n_cells_ * n_control_points_ * param_.rel_primal_eps);
        double dual_threshold =   std::max(param_.abs_eps, n_edges_ * n_control_points_ * param_.rel_dual_eps);
        primal_residual_sqr_norm_threshold_ =  primal_threshold * primal_threshold;
        dual_residual_sqr_norm_threshold_ =    dual_threshold * dual_threshold;
        std::cout <<"prime threshold : " << primal_residual_sqr_norm_threshold_ << std::endl;
        std::cout << "dual threshold : " << dual_residual_sqr_norm_threshold_ << std::endl;
    }
    void fastLBCSolver::init_linear_constraints()
    {
        Kt.resize(dim_ + 1, n_control_points_);

        Kt.block(0, 0, dim_, n_control_points_) = control_points_;
        Kt.row(dim_).fill(1.0);

       // Eigen::JacobiSVD<DenseMatrix, Eigen::FullPivHouseholderQRPreconditioner> jsvd(Kt, Eigen::ComputeFullU | Eigen::ComputeFullV);
        svd_Kt.compute(Kt, Eigen::ComputeFullU | Eigen::ComputeFullV);

        int nrank = svd_Kt.nonzeroSingularValues();
        M_ = svd_Kt.matrixV().block(0, nrank, n_control_points_, n_control_points_ - nrank);
        Mt_ = M_.transpose();

        svd_M.compute(M_, Eigen::ComputeFullU | Eigen::ComputeFullV);
        QR_M.compute(M_);
        LDLT_M.compute(Mt_ * M_);
        //compute the d_jk_prime in update - D subproblem
         D_prime.resize(n_inner_edges, n_control_points_);
        for(int i = 0 ; i < n_inner_edges; i ++)
        {
            int e_index,p1_index,p2_index;
            Vector3d b;
            e_index = inner_edges_[i];
            p1_index =  edge_vertices_(0, e_index);
            p2_index =  edge_vertices_(1, e_index);

            b(0) = sample_points_(0, p1_index) - sample_points_(0, p2_index);
            b(1) = sample_points_(1, p1_index) - sample_points_(1, p2_index);
            b(2) = 0;
            D_prime.block(i, 0, 1, n_control_points_) = svd_Kt.solve(b).transpose();
        }
    }


    void fastLBCSolver::update_G()
    {
        #pragma omp parallel for num_threads(8)

        for(int i = 0; i < n_cell_grad_; i ++)
        {

            int f_index = i % n_cells_;\
            int R_index = 3 * f_index;
            int r_index = 2 * f_index;
            int c_index = i / n_cells_;


            double alpha = Alpha_(f_index, c_index);
            double mu = param_.penalty_weight;
            double lambda = alpha / mu;
            Vector3d f;

            for(int j = 0; j < 3 ; j ++)
            {
              f(j) = D_(cell_edges_(f_index, j), c_index) - Lambda_(R_index + j, c_index) / mu;
            }
        //    f = SD_.block(R_index, c_index, 3, 1) - Lambda_.block(R_index, c_index, 3 ,1);

            Vector2d  v = VE.block(0, R_index, 2, 3) * f;
            Vector2d  g ;
            double D1 = eigvalue(0, f_index);
            double D2 = eigvalue(1, f_index);
            double V0 = eigvector(0, r_index);
            double V1 = eigvector(0, r_index + 1);
            double V2 = eigvector(1, r_index);
            double V3 = eigvector(1, r_index + 1);


            if(v.norm() <= lambda)
            {
                g(0) = 0;
                g(1) = 0;
            }
            else
            {
               double r = find_root(v, D1, D2, lambda);
               Vector2d x;
               x(0) = v(0) / (D1 + lambda / r);
               x(1) = v(1) / (D2 + lambda / r);
               g(0) = V0 * x(0) + V1 * x(1);
               g(1) = V2 * x(0) + V3 * x(1);
            }

            G_.block(r_index, c_index, dim_, 1) = g;

       }
    }
    Vector2d fastLBCSolver::solve_G(DenseMatrix& E, Vector3d& f, double& alpha, double& mu)
    {
        Vector2d g;
        DenseMatrix Id = Eigen::Matrix2d::Identity(2, 2);
        DenseMatrix A;
        Vector2d b;
        b = mu * E.transpose() * f;
        A = alpha * Id + mu * E.transpose() * E; // lambda = alpha / mu;
        g = A.inverse() * b;
        return(g);
    }
     double fastLBCSolver::find_root(Vector2d& v, double& D1, double& D2, double& lambda)
    {
        double r1 = (v.norm() - lambda) / D1;
        double r2 = (v.norm() - lambda) / D2;
        double r_t = std::min(r1, r2);
        double y1 = yvalue(v, D1, D2, lambda, r1);
        double y2 = yvalue(v, D1, D2, lambda, r2);
        if(std::fabs(y1 - y2) < 1e-9)
        {
            return ((r1 + r2) / 2);
        }
        else
        {
        double z1 = 1 / std::sqrt(y1 + 1);
        double z2 = 1 / std::sqrt(y2 + 1);
        double r = (r2 - r1 + r1 * z2 - r2 * z1) / (z2 - z1);
        double f ;
        double df;
        int iter = 0;
        func(v, D1, D2, lambda, r, f, df);
        while(std::fabs(f) > 1e-9)
        {
            r = r - f / df;
            if(r < r_t)
            {
                r = r_t;
            }
            func(v, D1, D2, lambda, r, f, df);
            iter ++;
        }
        return(r);

        }

  }
double fastLBCSolver::yvalue(Vector2d& v, double& D0, double& D1,double& lambda, double& x)
{
    double y0, y1;
    double z0, z1;
    y0 = D0 * x + lambda;
    y1 = D1 * x + lambda;

    z0 = v(0) / y0;
    z1 = v(1) / y1;

   return(z0 * z0 + z1 * z1 - 1);

}
 void fastLBCSolver::func(Vector2d& v, double& D1, double& D2, double& lambda, double& x,double& f, double& df)
    {
        double y0, y1;
        double z0, z1;
        y0 = D1 * x + lambda;
        y1 = D2 * x + lambda;

        z0 = v(0) / y0;
        z1 = v(1) / y1;

        f = z0 * z0 + z1 * z1 - 1;
        df = -2 * (D1 * z0 * z0 / y0 + D2 * z1 * z1 / y1);
    }
    Vector2d fastLBCSolver::mosek_solve(DenseMatrix E, Vector3d f, double alpha, double mu)
    {
        double start_time;
        double end_time;

        auto pointer_E =  std::shared_ptr<ndarray<double, 1>>(new ndarray<double, 1>(6));
        auto pointer_f =  std::shared_ptr<ndarray<double, 1>>(new ndarray<double, 1>(3));
        for(int i = 0; i < 3; i ++)
        {
            for(int j = 0; j < 2; j ++)
            {
               (*pointer_E)[2 * i + j] = E(i, j);
            }
            (*pointer_f)[i] = f(i);
        }

        Matrix::t  data_E = Matrix::dense(3, 2, pointer_E);
        Matrix::t  data_f = Matrix::dense(3, 1, pointer_f);

        Model::t M = new Model("Mosek Solver");
        auto _M = finally([&]() {M -> dispose();});

        auto y = M->variable("y", 1, Domain::unbounded());
        auto x = M->variable("x", 2, Domain::unbounded());
        auto z = M->variable("z", 1, Domain::unbounded());
        auto temp = Expr::sub(Expr::mul(data_E, x), data_f);
        M->constraint(Expr::vstack(1.,z,temp),Domain::inRotatedQCone());
        M->constraint(Expr::vstack(y, x), Domain::inQCone());
        M->objective(ObjectiveSense::Minimize, Expr::add(Expr::mul(alpha, y), Expr::mul(mu, z)));
        M->setSolverParam("intpntCoTolDfeas",1e-3);
        M->setSolverParam("intpntCoTolPfeas", 1e-3);
        //M->setSolverParam("intpntTolPsafe", 10);
        //M->setLogHandler([](const std::string &msg){ std::cout << msg << std::flush;});
        start_time = omp_get_wtime();
        M->solve();
        auto xx = *(x->level());
        Vector2d g ;
        g(0) = xx[0]; g(1) = xx[1];
        //std::cout << "result : \n" << g(0) << std::endl << g(1) << std::endl;

        end_time = omp_get_wtime();

        std::cout << "cost_time :" << std::max(0.0, end_time - start_time) << std::endl;

        return(g);
    }

    Vector2d fastLBCSolver::MM_algorithm( DenseMatrix E, Vector3d f, double alpha, double mu)
    {
        Vector2d temp_g;
        Vector2d g;
        Vector2d   result;
        DenseMatrix coef;
        DenseMatrix Id;
        DenseMatrix E_;
        bool is_convergence;
        double epsilon = 1e-9;
        g(0) =1; g(1) = 0;
        coef.resize(2, 2);
        Id = Eigen::Matrix2d::Identity(2,2);
        is_convergence = false;
        int iter = 0;
        double det;
        E_ = E.transpose() * E;
        while(! is_convergence)
        {
            temp_g = g;
            coef = alpha * Id + mu * g.norm() * E_;
            det = coef(0, 0) * coef(1, 1) - coef(0, 1) * coef(1, 0);
            result = mu * g.norm() * E.transpose() * f;
            g(0) = (result(0) * coef(1,1) - result(1) * coef(0, 1)) / det;
            g(1) = (result(1) * coef(0,0) - result(0) * coef(1, 0)) / det;
            if((temp_g - g).norm() < epsilon)
            {
                is_convergence = true;
            }
            iter ++ ;
        }

        //using method 2  (initialize using last step)
        /*is_convergence = false ;
        iter = 0;
        while(! is_convergence)
        {
            temp_g = g0;
            coef = alpha * Id + mu * g0.norm() * E_;
            result = mu * g0.norm() * E.transpose() * f ;
            det = coef(0, 0) * coef(1, 1) - coef(0, 1) * coef(1, 0);
            g0(0) = (result(0) * coef(1,1) - result(1) * coef(0, 1)) / det;
            g0(1) = (result(1) * coef(0,0) - result(0) * coef(1, 0)) / det;
            if((temp_g - g0).norm() < epsilon)
            {
                is_convergence = true;
            }
            iter ++;
        }
        std::cout << "iterations 2 : " << iter << std::endl;*/
        return (g);
    }

    void fastLBCSolver :: update_D()
    {
      #pragma omp parallel for num_threads(8)
        for(int i = 0; i < 3 * n_cells_; i ++)
        {
            int cell_index = i / 3;
            int edge_index = i % 3;
            EG_.row(i) = param_.relaxation_alpha* E_.block(i, 2 * cell_index, 1, 2) * G_.block(2 * cell_index, 0, 2, n_control_points_)
                    - (param_.relaxation_alpha - 1) * D_.row(cell_edges_(cell_index, edge_index));
        }


       #pragma omp parallel for num_threads(8)
        for(int i = 0 ; i < n_inner_edges; i ++)
        {
            double mu = 1 / param_.penalty_weight;

            DenseVector d_jk_prime;
            DenseVector sum_jk;
            DenseVector y_jk;
            sum_jk.setZero(n_control_points_, 1);
            d_jk_prime = D_prime.row(i).transpose();
            for(int j = 0; j < 2; j ++)
            {
              int c_index = edge_E_(inner_edges_[i], j);
              int r_index = c_index / 3  * 2;
              sum_jk += (EG_.row(c_index) + mu * Lambda_.row(c_index)).transpose();
            }
             y_jk = svd_M.solve(0.5 * sum_jk - d_jk_prime);
             D_.row(inner_edges_[i]) = (d_jk_prime + M_ * y_jk).transpose();
         }
    }

    void fastLBCSolver::update_dual_variables(int iter_num)
    {
    #pragma omp sections
        {
       #pragma omp section
          {
            if(check_convergence_)
              {
                 primal_residual_ = (E_ * G_ - S_ * D_);
               }

          }
       #pragma omp section
          {
            if(check_convergence_)
             {
                dual_residual_ = param_.penalty_weight * (D_ - previous_D);
              }
           }
       #pragma omp section
         {
            Lambda_ += param_.penalty_weight*(EG_ - S_ * D_);
           }
        }

    #pragma omp single
        {

        if(check_convergence_){
              primal_residual_sqr_norm_ = (primal_residual_).squaredNorm();
              dual_residual_sqr_norm_   =(dual_residual_).squaredNorm();
              optimization_converge_ = ( primal_residual_sqr_norm_ <= primal_residual_sqr_norm_threshold_
                        && dual_residual_sqr_norm_ <= dual_residual_sqr_norm_threshold_ );

              optimization_end_ = optimization_converge_ || iter_num >= param_.max_iterations;

                if(optimization_converge_){
                    std::cout << "Solver converged." << std::endl;

                }
                else if(optimization_end_){
                    std::cout << "Maximum iteration reached." << std::endl;
                }

                if (output_progress_ || optimization_end_)
                {
                    restore_prime_residual.push_back(primal_residual_sqr_norm_);
                    restore_dual_residual.push_back(dual_residual_sqr_norm_);
                    std::cout << "Iteration " << iter_num << ":" << std::endl;
                    std::cout << "Primal residual squared norm: " << primal_residual_sqr_norm_ << ",  primal threshold:" << primal_residual_sqr_norm_threshold_ << std::endl;
                    std::cout << "Dual residual squared norm: " << dual_residual_sqr_norm_ << ",  dual threshold:" << dual_residual_sqr_norm_threshold_ << std::endl;

                }
            }
    }
    }

    void fastLBCSolver ::solve()
    {
        start_timer();
        int iter = 0;
        optimization_end_ = false;
       while (!optimization_end_)
        {
            iter++;
            check_convergence_ = (iter % convergence_check_frequency_ == 0 || iter >= param_.max_iterations);
            output_progress_ = (iter % output_frequency_ == 0);
            previous_D = D_;
            {

                this -> update_G();

                this -> update_D();

                this -> update_dual_variables(iter);

            }    
        }
        end_timer();
        show_elapsed_time();
        get_final_coordinates();
    }

    void fastLBCSolver::get_final_coordinates()
    {
        int max_index_boundary_point = n_sample_points_ - n_data_points_;
        DenseMatrix  b(n_inner_edges, n_control_points_);
        b.fill(0.0);
        for(int i = 0; i < n_inner_edges; i ++)
        {
            int index_p1,index_p2;
            index_p1 =std::min(edge_vertices_(0 , inner_edges_[i]), edge_vertices_(1, inner_edges_[i]));
            index_p2 =std::max(edge_vertices_(0 , inner_edges_[i]), edge_vertices_(1, inner_edges_[i]));
            if(index_p1 < max_index_boundary_point && index_p2 >= max_index_boundary_point)
            {
                b.row(i) = D_.block(inner_edges_[i], 0, 1, n_control_points_) - init_coord_.row(index_p1);
            }
            if(index_p1 >= max_index_boundary_point && index_p2 >= max_index_boundary_point)
            {
                b.row(i) = D_.block(inner_edges_[i], 0, 1, n_control_points_);
            }
        }

        Z_ = Coef_solver.solve(Coef_.transpose() * b);
    }


    void fastLBCSolver:: compute_energy()
    {
        double energy1 = 0;
        double energy2 = 0;
        double energy3 = 0;
        DenseMatrix gradient = G_;
        DenseMatrix residual = E_ * G_ - S_ * D_ ;
        double l2_norm ;
        for(int i = 0; i < n_cells_; i ++)
        {
            for(int j = 0; j < n_control_points_; j ++)
            {
                l2_norm = sqrt((pow(gradient(2 * i , j), 2) + pow(gradient(2 * i + 1 ,j), 2)));
                energy1 = energy1 + grad_weights_(i, j) * l2_norm * A_(i) ;
            }
        }
        for(int i =0; i < 3 * n_cells_; i ++)
        {
            for(int j = 0; j < n_control_points_; j ++)
            {
                energy2 = energy2 + Lambda_(i, j) * residual(i, j);
            }
        }
        energy3 = param_.penalty_weight * residual.squaredNorm();
    }

    void fastLBCSolver::start_timer(){
        if(param_.use_timer){
            start_time_ = omp_get_wtime();
        }
    }

    void fastLBCSolver::end_timer(){
        if(param_.use_timer){
            end_time_ = omp_get_wtime();
        }
    }

    void fastLBCSolver::show_elapsed_time(){
        if(param_.use_timer){
            std::cout << "Solving time: " << std::max(0.0, end_time_ - start_time_) << " seconds." << std::endl;
        }
    }
}


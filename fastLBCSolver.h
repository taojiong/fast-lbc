#ifndef FASTLBCSOLVER_H
#define FASTLBCSOLVER_H

#include <common.h>
#include <DataSetup.h>
#include <algorithm>


namespace LBC {

    struct fastParam{

        // The relaxation coefficient for ADMM
        double relaxation_alpha;

        // weight for the augmented Lagrangian function
        double penalty_weight;


        // Relative convergence threshold for primal and dual residuals
        double rel_primal_eps, rel_dual_eps;

        // Absolute threshold for primal and dual residuals
        double abs_eps;

        int max_iterations;

        // How often should we check the convergence of the solver?s
        int convergence_check_frequency;

        // How often should we output the progress of the solver?
        // This variable represents the ratio between the output frequency and the convergence check frequency
        int output_frequency_ratio;


        bool use_timer;



        // Constructor that sets the default values
        fastParam():
        relaxation_alpha(1.65), penalty_weight(10),rel_primal_eps(1e-6), rel_dual_eps(1e-6),
        abs_eps(1e-8), max_iterations(10000), convergence_check_frequency(10),
        output_frequency_ratio(10),
        use_timer(true)
        {}
    };

    class fastLBCSolver
    {
    public:
        fastLBCSolver(const fastParam &param, const DataSetup &data_setup):
                param_(param),sample_points_(data_setup.get_sample_point()),
                control_points_(data_setup.get_LBC_solver_control_points()),
                data_points_(data_setup.get_LBC_solver_data_points()),
                grad_weights_(data_setup.get_LBC_solver_grad_weights()),
                edge_vertices_(data_setup.get_edge_vertces()),cell_edges_(data_setup.get_cell_edges()),
                edge_E_(data_setup.get_edge_E()),inner_edges_(data_setup.get_inner_edge()),
                E_(data_setup.get_edge_matrix()),D_(data_setup.get_coord_edge_matrix()),
                A_(data_setup.get_cell_area()),  S_(data_setup.get_choose_matrix()),
                init_coord_(data_setup.get_init_coord()),
                has_init_coord_(false), start_time_(0.0), end_time_(0.0)
        {
            init();
        }
        virtual~ fastLBCSolver(){}
        const DenseMatrix& get_coordinates() const{
            return Z_;
        }
        void solve();
        DenseMatrix D_;
    protected:

        fastParam param_; //parameters

        DenseMatrix control_points_,data_points_,sample_points_;
        DenseMatrix grad_weights_;
        DenseIndexMatrix edge_E_;
        DenseIndexMatrix edge_vertices_;
        DenseIndexMatrix cell_edges_;
        std::vector<int> inner_edges_;

        DenseMatrix   init_coord_;
        DenseMatrix   Z_ ;
        SparseMatrix  E_;
        SparseMatrix  S_;
        DenseMatrix   G_;
        DenseMatrix   Lambda_;     // dual variable  (its size is same with D_)
        DenseMatrix   M_,Mt_,Kt;     // null space of linear constraints
        DenseVector   A_;           // area of each cell
        SparseMatrix Coef_;         // coefficient matrix for construction
        DenseMatrix  Alpha_;        // update G subproblem coefficient matrix
        DenseMatrix  D_prime;            // update D d_jk_prime

        bool has_init_coord_;
        double start_time_, end_time_;

        // problem dimensions
        int dim_;
        int n_sample_points_;
        int n_control_points_;
        int n_data_points_;
        int n_cells_;
        int n_edges_;
        int n_inner_edges;
        int n_cell_edges_;  // A triangle have three edges
        int n_cell_grad_  ; // Number of gradients of total triangle cells


        //restore prime_residual and dual_residual

        std::vector<double> restore_prime_residual,restore_dual_residual;

        // Variables for primal and dual residuals
        // prime_residual = E*G-D, dual_residual = mu*(delta_D);
        DenseMatrix primal_residual_, dual_residual_, previous_D, previous_Lambda;
        double primal_residual_norm_, dual_residual_norm_;
        double primal_residual_sqr_norm_, dual_residual_sqr_norm_;
        double primal_residual_sqr_norm_threshold_, dual_residual_sqr_norm_threshold_;


        // Cholesky solver for the linear system
        #ifdef USE_CHOLMOD
        Eigen::CholmodDecomposition<SparseMatrix> solver_;
        #else
        Eigen::LDLT<DenseMatrix> solver_M,solver_Kt;
        #endif

        bool optimization_converge_, optimization_end_;

        bool check_convergence_;
        bool output_progress_;
        int output_frequency_, convergence_check_frequency_;

        // update step for G & D
        void update_G();
        void update_D();
        //update step for dual _variable and check its convergence
        void update_dual_variables(int iter_num);

        //init variables and threshold
        void init_variable();
        void init_threshold();
        void init_linear_constraints();
        void init();
        Vector2d MM_algorithm(DenseMatrix, Vector3d, double, double);  //MM step for updating G

        Vector2d mosek_solve(DenseMatrix, Vector3d, double, double);

        Vector2d solve_G(DenseMatrix&, Vector3d&, double&, double&);


        void func(Vector2d&, double&, double&, double&, double&, double&, double&);
        double yvalue(Vector2d&, double&, double&, double&, double&);
        double  find_root(Vector2d&, double&, double&, double&);
        DenseMatrix eigvalue;
        DenseMatrix eigvector;
        DenseMatrix VE; // V * E

        void  get_final_coordinates(); //   get final barycentric coordinates
        //
        // Timer methods
        void start_timer();
        void end_timer();
        void show_elapsed_time();

        void get_edge_difference();
        void compute_energy();
        Eigen::JacobiSVD< DenseMatrix, Eigen::FullPivHouseholderQRPreconditioner >svd_Kt;
        Eigen::JacobiSVD< DenseMatrix, Eigen::FullPivHouseholderQRPreconditioner >svd_M;
        //Eigen::ColPivHouseholderQR<DenseMatrix> QR_M;
        Eigen::HouseholderQR<DenseMatrix> QR_M;
        //Eigen::SparseQR <SparseMatrix, Eigen::COLAMDOrdering<int>> Coef_solver;
        Eigen::SimplicialLDLT<SparseMatrix> Coef_solver;
        Eigen::LDLT<DenseMatrix> LDLT_M;
        int sum_iter = 0 ;
        double start_time;
        double end_time;
        std::vector<double> cost_time;
        double total_time = 0;
        DenseMatrix EG_; // E * G(for over relaxation)
        DenseMatrix SD_; // S * D
        std::vector<double> prime_error;
        std::vector<double> dual_error;
    };



}

#endif // FASTLBCSOLVER_H
